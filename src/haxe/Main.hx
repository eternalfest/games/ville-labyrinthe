import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import better_script.DeadlyBottom;
import better_script.Sprites;
import atlas.Atlas;
import pretty_portals.PrettyPortals;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	gameParams: GameParams,
	noNextLevel: NoNextLevel,
    deadlyBottom: DeadlyBottom,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    pretty_portals: PrettyPortals,
    sprites: Sprites,
	merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
