package pretty_portals;

import pretty_portals.actions.OpenPortalBack;

import merlin.IAction;
import patchman.IPatch;
import patchman.Ref;


@:build(patchman.Build.di())
class PrettyPortals {
    @:diExport
    public var openPortalBack(default, null): IAction;

    public function new(): Void {
        this.openPortalBack = new OpenPortalBack();
    }
}
