package pretty_portals.actions;

import patchman.DebugConsole;
import patchman.DebugConsole;
import etwin.flash.filters.ColorMatrixFilter;
import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;

class OpenPortalBack implements IAction {
    public var name(default, null): String = Obfu.raw("openPortalBack");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var cx: Float = ctx.getFloat(Obfu.raw("x"));
        var cy: Float = ctx.getFloat(Obfu.raw("y"));
        var pid: Int = ctx.getInt(Obfu.raw("pid"));

        var game: hf.mode.GameMode = ctx.getGame();
        if (game.portalMcList[pid] != null) {
            return false;
        }
        game.world.scriptEngine.insertPortal(cx, cy, pid);
        var v5 = game.root.Entity.x_ctr(game.flipCoordCase(cx));
        var v6 = game.root.Entity.y_ctr(cy) - game.root.Data.CASE_HEIGHT * 0.5;
        var view = game.world.view;
        view._back_dm.destroy();
        view._back.bitmapMC = view._back_dm.empty(0);
        view.attachBg();
        var v7 = view._back_dm.attach("hammer_portal", 0);
        view._back.attachBitmap(view.tileCache, view._back.getNextHighestDepth());
        v7._x = v5 + view.xOffset;
        v7._y = v6;
        game.fxMan.attachExplosion(v5, v6, 40);
        game.fxMan.inGameParticles(game.root.Data.PARTICLE_PORTAL, v5, v6, 5);
        game.fxMan.attachShine(v5, v6);
        game.portalMcList[pid] = {x: v5, y: v6, mc: v7, cpt: 0};

        return false;
    }
}
